[![pipeline status](https://git.coop/aptivate/ansible-roles/set-hostname/badges/master/pipeline.svg)](https://git.coop/aptivate/ansible-roles/set-hostname/commits/master)

# set-hostname

Sets a machine's hostname. This differs from Ansible's hostname as it also updates /etc/hosts

# Role Variables

- **host_fqdn** (required): The fully qualified domain name of the machine
- **host_ip** (required): The ip address of the machine

# Example Playbook

```yaml
- hosts: localhost
  roles:
     - {role: set-hostname, host_fqdn: "dummy.aptivate.org", host_ip: "192.0.0.8"}
```

# Testing

This is tested using Vagrant driver only at the moment.

```bash
  $ pipenv install --dev
  $ pipenv run molecule test
```

# License

  * https://www.gnu.org/licenses/gpl-3.0.en.html

# Author Information

  * https://aptivate.org/
  * https://git.coop/aptivate
