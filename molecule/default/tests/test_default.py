import os
import re

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_hostname_is_set(host):
    hostname = host.check_output("hostname")
    assert hostname.strip() == 'dummy.aptivate.org'


def test_host_file_contains_host(host):
    f = host.file('/etc/hosts')
    expected = '^192.0.0.8\s+dummy.aptivate.org$'
    assert re.search(expected, f.content, re.M) is not None
